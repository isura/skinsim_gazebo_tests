# Gazebo Physics Tests

# About
This code performs tests on Gazebo related to SkinSim development.

# Install

git clone https://isura@bitbucket.org/isura/skinsim_gazebo_tests.git

mkdir skinsim_gazebo_tests/build
cd skinsim_gazebo_tests/build

Compile the code.

cmake ../
make

# Setup Environment Variables

export GAZEBO_MODEL_PATH=$GAZEBO_MODEL_PATH:~/skinsim_gazebo_tests/models

export GAZEBO_PLUGIN_PATH=$GAZEBO_PLUGIN_PATH:~/skinsim_gazebo_tests/build

# Run
gazebo -u ../worlds/spring_array_10.world

# Build Status
[![Build Status](https://drone.io/bitbucket.org/isura/skinsim_gazebo_tests/status.png)](https://drone.io/bitbucket.org/isura/skinsim_gazebo_tests/latest)